"""
"""
import os
import numpy as np

data_filename = os.path.join("data", "diamonds.csv")


class Data:
    def __init__(self, repeat=False):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

        with open(data_filename, "rt") as f:
            data_lines = f.readlines()
            # The first row is full of column labels.
            # column_labels = data_lines[0].split(",")

        self.n_features = 9
        all_features = []
        all_labels = []

        # Create ordered lists for cut, color, and clarity, so that their values
        # can be converted into numbers based on their position in the list.
        # column 2 cut
        col2_labels = [
            '"Fair"', '"Good"', '"Very Good"', '"Premium"', '"Ideal"']
        # column 3 color
        col3_labels = ['"J"', '"I"', '"H"', '"G"', '"F"', '"E"', '"D"']
        # column 4 clarity
        col4_labels = [
            '"I1"', '"SI2"', '"SI1"', '"VS2"', '"VS1"',
            '"VVS2"', '"VVS1"', '"IF"']

        with open(data_filename, "rt") as f:
            data_lines = f.readlines()
            # column_labels = data_lines[0].split(",")

        raw_data = []
        # Ignore the row with the column labels.
        for line in data_lines[1:]:
            line_strings = line.split(",")
            # Convert cut, color, and clarity strings into numbers, based
            # on their position in their respective lists.
            line_strings[2] = col2_labels.index(line_strings[2])
            line_strings[3] = col3_labels.index(line_strings[3])
            line_strings[4] = col4_labels.index(line_strings[4])
            # Leave off the row number by starting at column 1.
            # Convert everything to floats.
            line_nums = [float(val) for val in line_strings[1:]]
            raw_data.append(line_nums)
        raw_data = np.array(raw_data)

        # Segregate out the features from the prices.
        self.features = raw_data[:, [0, 1, 2, 3, 4, 5, 7, 8, 9]]
        self.labels = raw_data[:, 6]

        # Perform a log transformation on feature 0 (carats)
        # and the label (price).
        # Two reasons for this:
        # 1) There is a lot more data at lower carats and prices. This nudges
        #    the data in the direction of uniformly distributed. While k-NN
        #    doesn't need any particular distribution to work, this helps
        #    distances (differences in carats) to be more consistent across its
        #    range.
        # 2) Differences in price, in particular, make sense to represent as
        #    percents, rather than absolute values.
        #    Being $50 off a $500 diamond
        #    is very different than for a $50,000 diamond. A 2% error is more
        #    meaningful across diamonds of widely difference prices. The log
        #    transformation gets us this.
        self.features[:, 0] = np.log10(self.features[:, 0])
        self.labels = np.log10(self.labels)

        # Put the data points in a random order.
        i_order = np.arange(self.labels.size)
        np.random.shuffle(i_order)
        self.features = self.features[i_order, :]
        self.labels = self.labels[i_order]
        self.i_diamond = 0
        self.repeat = repeat

    def __str__(self):
        str_parts = [
            "Diamond price data set",
            f"diamonds represented: {self.labels.size}",
            f"feature count: {self.features.shape[1]}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if self.i_diamond >= self.labels.size:
            if self.repeat:
                i_order = np.arange(self.labels.size)
                np.random.shuffle(i_order)
                self.features = self.features[i_order, :]
                self.labels = self.labels[i_order]
                self.i_diamond = 0
            else:
                raise StopIteration

        self.forward_out = (
            self.features[self.i_diamond, :], self.labels[self.i_diamond])
        self.i_diamond += 1
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        return self.backward_out


if __name__ == "__main__":
    diamonds = Data(repeat=False)
    print(diamonds)
    for i in range(10):
        print(diamonds.forward_pass(None))
