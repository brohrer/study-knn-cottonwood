"""
This code makes use of the Cottonwood machine learning framework.
Get it at https://e2eml.school/cottonwood
"""
import numpy as np
import matplotlib.pyplot as plt
from cottonwood.structure import Structure
from knn_d import KNN
from diamonds import Data

k = 5
n_iter = 40000
n_bin = 1000
differences = []
weight_adjustment_interval = 10

model = Structure()
model.add(Data(), "data")
model.add(KNN(
    k=k,
    is_classifier=False,
    # weight_adjustment_wait=1,
    weight_adjustment_interval=weight_adjustment_interval,
), "knn")

model.connect("data", "knn")
model.connect("data", "knn", i_port_tail=1, i_port_head=1)
for i_iter in range(n_iter):
    print(f"iter {i_iter}", end="\r")
    model.forward_pass()
    try:
        differences.append(np.abs(model.blocks["knn"].target_label -
                           model.blocks["knn"].forward_out))
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

differences = np.array(differences[k + 1:])
differences = differences[:(differences.size // n_bin) * n_bin]
differences = differences.reshape(n_bin, -1)
mean_differences = np.mean(differences, axis=0)
pct = (10 ** mean_differences - 1) * 100

iters = np.arange(mean_differences.size) * n_bin
fig = plt.figure()
ax = fig.gca()
ax.plot(iters, pct, color="#04253a")
ax.set_title(f"k-NN with Diamond Prices, k = {model.blocks['knn'].k}")
ax.set_xlabel("Iteration")
ax.set_ylabel("Mean percentage price error")
ax.grid()
plt.savefig("learning_diamond_weights.png", dpi=300)
plt.show()
